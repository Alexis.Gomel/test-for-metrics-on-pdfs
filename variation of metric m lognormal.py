# -*- coding: utf-8 -*-
"""
Created on Fri May 18 17:46:57 2018

Metrics under the lognormal distribution


@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
import metrics
import scipy.stats as st
import statplot as stp
import astropy.stats as astrost
#import astropy.stats.knuth_bin_width as knuth
from scipy.interpolate import griddata
from mpl_toolkits.mplot3d import Axes3D
import boots
k=0.2
#%%
# =============================================================================
# Lognormal distribution
# =============================================================================
steps=300
y=np.linspace(0.01,7.0,steps)
M=np.zeros(steps)
kr2=np.zeros(steps)
rri=np.zeros(steps)
kut=np.zeros(steps)
i=0
sample=40000
keep_stat=np.zeros([steps,sample])
for sigma in y:
    s = np.random.lognormal(1,sigma, sample)
    M[i]=metrics.RTW_max(s, k)
    kr2[i]=metrics.kr2(s)
    rri[i]=metrics.metric_rri(s)
    kut[i]=st.kurtosis(s)
    keep_stat[i]=s
    i=i+1

#%%
fig=plt.figure()
s = np.random.lognormal(1,4, 40000)
ax=fig.add_subplot(111)
height,bin_pos=astrost.histogram(s,bins='blocks',density=True)
ax.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='red',align='edge',alpha=0.5,label='Solutions')
ax.set_yscale('log')

#%%
fig1=plt.figure()
fig1.suptitle("lognormal value as a funtion of $\sigma$" , fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(221)
#plt.plot(y,M,label='pareto' )
plt.plot(y,M,label='pareto')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('M')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  
#fig2=plt.plot(np.ones(len(s)),s,'.')


ax2 = fig1.add_subplot(222)
#plt.plot(y,M,label='pareto' )
plt.plot(y,kr2,label='kr2')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('KR2')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  

ax2 = fig1.add_subplot(223)
#plt.plot(y,M,label='pareto' )
plt.plot(y,rri,label='rri')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('RRI')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  

ax2 = fig1.add_subplot(224)
#plt.plot(y,M,label='pareto' )
plt.plot(y,kut,label='kurtosis')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('Kurtosi')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=1$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  


#%%
# number_of_samples = 220
# confidence = 60
# conf_interval=(0.5-0.01*confidence/2,.5+0.01*confidence/2)


# # confi=[np.percentile(boot_stat,conf_interval[0]*100),np.percentile(boot_stat,conf_interval[1]*100)]

# logref=np.random.lognormal(1,0.5, 15000)
# boot_stat,boot_error = boots.bootstrap_func_len(st.kurtosis,logref,number_of_samples)
# mean_boot=np.mean(boot_stat)
#  #%%
# e=np.e
# sig=0.5
# sig2=sig**2
# real_kur=e**(4*sig2)+2*e**(3*sig2)+3*e**(2*sig2)-6
# kr2ref=metrics.kr2(logref)
# Mref=metrics.RTW_max(logref, k)
# kurref=st.kurtosis(logref)
# rri_ref=metrics.metric_rri(s)

#%%

#%%
# bin_num=50
# # stp.pcolor_stat(s, bin_num)

# method='knuth'

# '''set up  pcolor figures with std'''
# std=0.1
# lims=0.4
# side=6
# amount_of_stats=steps#○so it always includes 0
# binss=np.linspace(-0.6,0.6,bin_num+1)

# avg_stat_bin=np.zeros((amount_of_stats))
# std_stat_bin=np.zeros((amount_of_stats))
# testlim=.50
# test_points=np.linspace(-testlim,testlim,amount_of_stats)
# j=0
# num_points=sample

# M=[]
# bcount=[]
# bval=[]

# for n,results in enumerate(keep_stat):
        
#     res_ct=astrost.histogram(results,bins=method,density=True)
#     bcount.append(res_ct[0])
#     bval.append(res_ct[1][:-1])
#     mean=np.mean(results)
#     M.append(mean*np.ones_like(res_ct[1][:-1]))
#     avg_stat_bin[n]=np.mean(results)
 
#     #%%
# bval_flat=[item for sublist in bval for item in sublist]
# M_flat=[item for sublist in M for item in sublist]
# bcount_flat=[item for sublist in bcount for item in sublist]
# #%%
# fig2=plt.figure(figsize=(10,10))
# # fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
# ax = fig2.add_subplot(111, projection='3d')
# ax.scatter(bval_flat,M_flat,bcount_flat)
# # ax.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
# ax.set_ylabel('$<y_0>$')
# #%%
# xi = np.linspace(min(binss),max(binss),150)
# yi = np.linspace(-testlim,testlim,150)
# zi = griddata((bval_flat,M_flat ),bcount_flat, (xi[None,:], yi[:,None]), method='cubic')

# fig2=plt.figure(figsize=(10,10))
# # fig2.suptitle('Statistics scatter for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the standar deviation.' %(r, r_std,num_points,std), fontsize=11, fontweight='bold')
# ax2=fig2.add_subplot(1,1,1)
# #plt.contourf(bval,M,bcount)
# #plt.contour(xi,yi,zi)
# plt.pcolor(xi,yi,zi)
# #plt.pcolor(xi,yi,np.log(zi))
# plt.plot(avg_stat_bin,test_points,'--r',alpha=0.8)
# # ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
# ax2.set_ylabel('$<y_0>$')
# plt.colorbar()
