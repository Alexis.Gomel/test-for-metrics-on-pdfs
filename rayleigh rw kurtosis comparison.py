# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 19:00:17 2018
rayleigh
@author: gomel
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jul  5 18:05:36 2018

weibull comparison between RW and Kurtosis

@author: gomel
"""



import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import threshold

import metrics

k=0.2

# =============================================================================
# rayleigh
# =============================================================================
steps=300
y=np.linspace(0.3,10.,steps)
M=np.zeros(steps)
kr2=np.zeros(steps)
kur=np.zeros(steps)
stand=np.zeros(steps)
RW=np.zeros(steps)
RW2=np.zeros(steps)
data=90000
i=0

mu=0.
for a in y:
    s=np.random.rayleigh(a, data)
    M[i]=metrics.RTW_max(s,k)
    kr2[i]=metrics.kr2(s)
    kur[i]=stats.kurtosis(s)
    stand[i]=np.std(s)
    rw_count=0
    rw2_count=0
    thres=2*threshold.sig_wave_3(s)
    thres2=2*threshold.sig_wave_3sort(s)

    for j in s:
        if j>thres:
           rw_count=rw_count+1
#            print(rw_count)
        if j>thres2:
           rw2_count=rw2_count+1
#            print(rw_count)
    RW[i]=rw_count*3000/data
    RW2[i]=rw2_count*3000/data
    i=i+1
    
#%%

fig1=plt.figure()
fig1.suptitle("Rayleigh  M value as a funtion of $a$", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
plt.plot(y,M,label='pareto')
plt.plot(y,kr2,label='kr2')
ax2.set_xlabel('$a$')
ax2.set_ylabel('$M$')
plt.legend()

plt.text(-0.1,-.2, "Parameters: pareto threshold ($k$)= %.2f " % (k), fontsize=11, transform=ax2.transAxes)  
#fig2=plt.plot(np.ones(len(s)),s,'.')


#%%
fig, ax1 = plt.subplots()
fig.suptitle("Rayleigh kurtosis and RW as a funtion of $a$", fontsize=10, fontweight='bold')
#ax1 = fig.add_subplot(111)

color = 'tab:red'
ax1.plot(y,kur,label='kurtosis',color=color)
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$K$',color=color)
ax1.set_xlabel('$a$')


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,RW,label='rw',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RW norm$',color=color)
####################################################################3
fig, ax1 = plt.subplots()
fig.suptitle("Rayleigh kurtosis and RW2 as a funtion of $a$", fontsize=10, fontweight='bold')
#ax1 = fig.add_subplot(111)
color = 'tab:red'
ax1.plot(y,kur,label='kurtosis',color=color)
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$K$',color=color)
ax1.set_xlabel('$a$')

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,RW2,label='rw2',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RW2 norm$',color=color)

#fig.tight_layout()

#%%
fig1=plt.figure()
fig1.suptitle("Rayleigh  kurtosis as a funtion of $a$", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
plt.scatter(kur,RW,label='kurtosis')
ax2.set_xlabel('$K$')
ax2.set_ylabel('$RW$')
plt.legend()

#%%
fig1=plt.figure()
fig1.suptitle("Rayleigh  deviation as a funtion of $a$", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
plt.plot(np.sqrt(2-np.pi*0.5)*y,stand,label='stand')
ax2.set_xlabel('$a$')
ax2.set_ylabel('$std$')
plt.legend()