# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 19:14:19 2018

@author: gomel
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import metrics as metrics
import boots

k=0.2

# =============================================================================
# rayleigh
# =============================================================================
steps=300
y=np.linspace(0.3,3,steps)
M=np.zeros(steps)
kr2=np.zeros(steps)
rc=np.zeros(steps)
rri=np.zeros(steps)
kur=np.zeros(steps)
i=0

stand=np.zeros(steps)
data=90000
i=0
ka, ti = 1.4, 1 #gamma's shape and scale


mu=0.
for i,a in enumerate(y):
    s=np.random.gamma(a, ti, data)
    M[i]=metrics.RTW_max(s, k)
    kr2[i]=metrics.kr2(s)
    rri[i]=metrics.metric_rri(s)
    kur[i]=stats.kurtosis(s)
    stand[i]=np.std(s)
    rc[i]=metrics.metric_rms_tld(s)
  
    
#%%

fig=plt.figure(10,figsize=(15,7))
fig.suptitle("Gamma M value as a funtion of 'shape' ($\sigma$) \n %1.f size sample " %(data), fontsize=10, fontweight='bold')
ax1=fig.add_subplot(121)
color = 'tab:red'
ax1.plot(y,M,label='RTW')
color = 'tab:red'
ax1.plot(y,M,label='RTW',color=color)
ax1.set_xlabel('$\sigma$')
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$RTW$',color=color)
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,rri,label='rri',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RRI$',color=color)
ax2 = fig.add_subplot(122)
ax2.scatter(M,rri,label='kurtosis')
ax2.set_xlabel('$RTW$')
ax2.set_ylabel('$RRI$')
#fig2=plt.plot(np.ones(len(s)),s,'.')
#%%
fig=plt.figure(11,figsize=(15,7))
fig.suptitle("Gamma M value as a funtion of 'shape' ($\sigma$) \n %1.f size sample " %(data), fontsize=10, fontweight='bold')
ax1=fig.add_subplot(121)
color = 'tab:red'
ax1.plot(y,M,label='RTW')
color = 'tab:red'
ax1.plot(y,M,label='RTW',color=color)
ax1.set_xlabel('$\sigma$')
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$RTW$',color=color)
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,rc,label='$rr_{rms}$',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RRI_{rms}$',color=color)
ax2 = fig.add_subplot(122)
ax2.scatter(M,rc)
ax2.set_xlabel('$RTW$')
ax2.set_ylabel('$RRI_{rms}$')
#fig2=plt.plot(np.ones(len(s)),s,'.')
fig.tight_layout()

#%%
fig=plt.figure(12,figsize=(15,7))
fig.suptitle("Gamma M value as a funtion of 'shape' ($\sigma$) \n %1.f size sample " %(data), fontsize=10, fontweight='bold')
ax1=fig.add_subplot(121)
color = 'tab:red'
ax1.plot(y,kur,label='kurtosis',color=color)
ax1.set_xlabel('$\sigma$')
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$Kurtosis$',color=color)
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,kr2,label='$Kr2$',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$kr_2$',color=color)
ax2 = fig.add_subplot(122)
ax2.scatter(kur,kr2)
ax2.set_xlabel('$kurtosis$')
ax2.set_ylabel('$Kr2$')
#fig2=plt.plot(np.ones(len(s)),s,'.')
# fig.tight_layout()

#%%
fig=plt.figure(13,figsize=(15,7))
fig.suptitle("Gamma M value as a funtion of 'shape' ($\sigma$) \n %1.f size sample " %(data), fontsize=10, fontweight='bold')
ax1=fig.add_subplot(121)
color = 'tab:red'
ax1.plot(y,rri,label='RRI',color=color)
ax1.set_xlabel('$\sigma$')
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$RRI$',color=color)
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,rc,label='$RRrms$',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RRrms$',color=color)
ax2 = fig.add_subplot(122)
ax2.scatter(rri,rc)
ax2.set_xlabel('$RRI$')
ax2.set_ylabel('$RRrms$')
#fig2=plt.plot(np.ones(len(s)),s,'.')
# fig.tight_layout()


#%%
fig=plt.figure(14,figsize=(15,7))

fig.suptitle("Gamma distribution kurtosis and RW as a funtion of 'shape' \n %i size sample " %(data), fontsize=10, fontweight='bold')
#ax1 = fig.add_subplot(111)
ax1=fig.add_subplot(121)

color = 'tab:red'
ax1.plot(y,kur,label='kurtosis',color=color)
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$K$',color=color)
ax1.set_xlabel('$\sigma$')


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,rri,label='rri',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RRI$',color=color)

#fig.tight_layout()


#%%
fig1=plt.figure(16)
fig1.suptitle("Gamma deviation as a funtion of $shape$", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
plt.plot(y,stand,label='stand')
ax2.set_xlabel('$a$')
ax2.set_ylabel('$std$')
plt.legend()


#%%
""" noisiness for metrics"""
data=100000
a=2.
ti=1.
s=np.random.gamma(a, ti, data)

step=200
points=np.arange(step,data+step,step)

plots=10
for j in np.arange(0,plots,1):
    
    kurtevol=np.empty_like(points, dtype='float')
    kr2evol=np.empty_like(points, dtype='float')
    mevol=np.zeros_like(points, dtype='float')
    rrievol=np.zeros_like(points, dtype='float')
    i=0
    for n in points:
        kurtevol[i]=stats.kurtosis(s[:n])
        kr2evol[i]=metrics.kr2(s[:n])
        datacosas=s[:n]
        mevol[i]=metrics.RTW_max(datacosas,k)
        rrievol[i]=metrics.metric_rri(datacosas)
        i=i+1
    
    fig=plt.figure(1)
    fig.suptitle('Kurtosis, Gamma distribution, $\sigma=$%.2f \n Kurtosis= %.3f for %.1f samples' %(a,kurtevol[-1],data), fontsize=12, fontweight='bold') 
    plt.plot(points,kurtevol/kurtevol[-1])
    
    fig=plt.figure(4)
    fig.suptitle('KR2, Gamma distribution, $\sigma=$%.2f\n KR2= %.3f for %.1f samples'%(a,kr2evol[-1],data), fontsize=12, fontweight='bold') 
    plt.plot(points,kr2evol/kr2evol[-1])
        
    fig=plt.figure(2)
    fig.suptitle('RRI, Gamma distribution, $\sigma=$%.2f\n RRI= %.3f for %.1f samples'%(a,rrievol[-1],data), fontsize=12, fontweight='bold') 
    plt.plot(points,rrievol/rrievol[-1])
     
    fig=plt.figure(3)
    fig.suptitle('RTW, Gamma distribution, $\sigma=$%.2f\n RTW= %.3f for %.1f samples'%(a,mevol[-1],data), fontsize=12, fontweight='bold') 
    plt.plot(points,mevol/mevol[-1])  

    np.random.shuffle(s)
    
    
# number_of_samples = 220
# confidence = 60
# conf_interval=(0.5-0.01*confidence/2,.5+0.01*confidence/2)

# boot_stat,boot_error = boots.bootstrap_func_len(stats.kurtosis,s,number_of_samples,debug=True)
# mean_boot=np.mean(boot_stat)
# confi=[np.percentile(boot_stat,conf_interval[0]*100),np.percentile(boot_stat,conf_interval[1]*100)]

# fig=plt.figure(1)
# plt.plot(points,mean_boot/kurtevol[-1],'--k',linewidth=1)

# boot_stat,boot_error = boots.bootstrap_func_len(metrics.kr2,s,number_of_samples,debug=True)
# mean_boot=np.mean(boot_stat)
# confi=[np.percentile(boot_stat,conf_interval[0]*100),np.percentile(boot_stat,conf_interval[1]*100)]
# fig=plt.figure(4)
# plt.plot(points,mean_boot/kr2evol[-1],'--k',linewidth=1)

# boot_stat,boot_error = boots.bootstrap_func_len(metrics.metric_rri,s,number_of_samples,debug=True)
# mean_boot=np.mean(boot_stat) 
# fig=plt.figure(2)
# plt.plot(points,mean_boot/rrievol[-1],'--k',linewidth=1)

# boot_stat,boot_error = boots.bootstrap_func_len(metrics.RTW_max,s,number_of_samples,debug=True)
# mean_boot=np.mean(boot_stat) 
# fig=plt.figure(3)
# plt.plot(points,mean_boot/mevol[-1],'--k',linewidth=1,label='')  


#%%
fig1=plt.figure(7)
fig1.suptitle("Gamma value as a funtion of $\sigma$" , fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(221)
#plt.plot(y,M,label='pareto' )
plt.plot(y,M,label='RTW')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('RTW')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  
#fig2=plt.plot(np.ones(len(s)),s,'.')

ax2 = fig1.add_subplot(222)
#plt.plot(y,M,label='pareto' )
plt.plot(y,kr2,label='kr2')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('KR2')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  

ax2 = fig1.add_subplot(223)
#plt.plot(y,M,label='pareto' )
plt.plot(y,rri,label='rri')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('RRI')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  

ax2 = fig1.add_subplot(224)
#plt.plot(y,M,label='pareto' )
plt.plot(y,kur,label='kurtosis')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('Kurtosis')
plt.legend()
# plt.text(-0.1,-.2, "Parameters: $\mu=1$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  
#%%
logref=np.random.gamma(4, 1, 39000)

kr2ref=metrics.kr2(logref)
Mref=metrics.RTW_max(logref, k)
kurref=stats.kurtosis(logref)
rri_ref=metrics.metric_rri(logref)
rri_ref/45

