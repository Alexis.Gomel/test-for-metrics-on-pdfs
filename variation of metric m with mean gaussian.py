# -*- coding: utf-8 -*-
"""
Created on Wed May 16 18:06:24 2018

Test to change shift on pareto metrics definitions
for a gaussian, translating the mean

@author: gomel
"""


import numpy as np
import matplotlib.pyplot as plt
import metrics

k=0.2

# =============================================================================
# Gaussian distribution
# =============================================================================
steps=400
sigma = 0.8 #standard deviation
y=np.linspace(-100.0,100.0,steps)
M=np.zeros(steps)
M_N=np.zeros(steps)
i=0
for mu in y:
    s = np.random.normal(mu, sigma, 500)
    M[i]=metrics.metricm(s,k)
    M_N[i]=metrics.RTW_max(s, k)
    i=i+1


fig1=plt.figure()
fig1.suptitle("Gaussian M value as a funtion of <x> ", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
plt.plot(y,M,label='pareto' )
plt.plot(y,M_N,label='new metric')
ax2.set_xlabel('$\mu (<x>)$')
ax2.set_ylabel('M')
plt.legend()
plt.text(-0.1,-.2, "Parameters: $\sigma=$ %.2f , pareto threshold ($k$)= %.2f " % (sigma, k), fontsize=11, transform=ax2.transAxes)  
#fig2=plt.plot(np.ones(len(s)),s,'.')
