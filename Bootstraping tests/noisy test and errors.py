# -*- coding: utf-8 -*-
"""
Created on Mon Sep 30 11:41:09 2019

Metric noise and error. 

@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import metrics
from sklearn.utils import resample
from time import sleep
import sys
import astropy.stats as astrost
import boots

#%%  end imports, start code.
'''Non parametric simple bootstraping'''
k=0.2
data=3000 #total amount of points
''' s is the distribution to be explored'''
s=np.random.rayleigh(2, data)
# s=np.random.exponential(2, data)

'''bootstraping parameters'''
number_of_samples = 220
confidence = 60
conf_interval=(0.5-0.01*confidence/2,.5+0.01*confidence/2)
conf_index=(int(number_of_samples*conf_interval[0]),int(number_of_samples*conf_interval[1]))
kurt=stats.kurtosis(s) #the statistic
kr2s=metrics.kr2(s) #the statistic
RTWs=metrics.RTW_max(s) #the statistic
#%%
'''bootstrapingkurt'''
#d = time()
boot_stat,boot_error = boots.bootstrap_func_len(stats.kurtosis,s,number_of_samples,debug=True)
#print('elapsed time for bootstraping: %.02f' % (time() - d))
      
################# end of bootstraping kurtosis ###########################
mean_boot=np.mean(boot_stat)
confi=[np.percentile(boot_stat,conf_interval[0]*100),np.percentile(boot_stat,conf_interval[1]*100)]

print( 'kurtosis=',kurt,\
      '\n mean kurt from bootstrap: ', mean_boot,\
      '\n error:',confi[0],confi[1] )

fig=plt.figure()
fig.suptitle('Bootstraping kurtosis histogram \n %.1f%% confidence result:  Kurtosis = %.2f + [%.2f,%.2f]'\
             %(confidence,kurt,confi[0]-mean_boot,confi[1]-mean_boot))
ax=fig.add_subplot(111)
height,bin_pos=astrost.histogram(boot_stat,bins='knuth',density=True)
ax.bar(bin_pos[:-1],height,width=np.diff(bin_pos),align='edge')
ax.plot((kurt,kurt),(0,np.max(height)),'-',color='orange',label='Empirical kurtosis from %i values' %(data))   
ax.plot((mean_boot,mean_boot),(0,np.max(height)),'-r',label='Mean resampled kurtosis')
ax.plot((confi[0],confi[0]),(0,np.max(height)),'--g',label='error')
ax.plot((confi[1],confi[1]),(0,np.max(height)),'--g')
ax.set_xlabel('Resampled Kurtosis')
plt.legend()

#%%

'''bootstraping kr2'''
#d = time()
boot_stat,boot_error = boots.bootstrap_func_len(metrics.kr2,s,number_of_samples,debug=True)
#print('elapsed time for bootstraping: %.02f' % (time() - d))     
################# end of bootstraping kurtosis ###########################
mean_boot=np.mean(boot_stat)
confi=[np.percentile(boot_stat,conf_interval[0]*100),np.percentile(boot_stat,conf_interval[1]*100)]

print( 'kr2=',kr2s,\
      '\n mean kr2 from bootstrap: ', mean_boot,\
      '\n error:',confi[0],confi[1] )
#%%
fig=plt.figure()
fig.suptitle('Bootstraping Kr2 histogram \n %.1f%% confidence result:  Kr2 = %.2f + [%.2f,%.2f]'\
             %(confidence,kr2s,confi[0]-mean_boot,confi[1]-mean_boot))
ax=fig.add_subplot(111)
height,bin_pos=astrost.histogram(boot_stat,bins='knuth',density=True)
ax.bar(bin_pos[:-1],height,width=np.diff(bin_pos),align='edge')
ax.plot((kr2s,kr2s),(0,np.max(height)),'-',color='orange',label='Empirical Kr2 from %i values' %(data))   
ax.plot((mean_boot,mean_boot),(0,np.max(height)),'-r',label='Mean resampled Kr2')
ax.plot((confi[0],confi[0]),(0,np.max(height)),'--g',label='error')
ax.plot((confi[1],confi[1]),(0,np.max(height)),'--g')
ax.set_xlabel('Resampled Kr2')
plt.legend()



#%%

'''bootstraping RTW'''
#d = time()
boot_stat,boot_error = boots.bootstrap_func_len(metrics.RTW_max,s,number_of_samples,debug=True)
#print('elapsed time for bootstraping: %.02f' % (time() - d))     
################# end of bootstraping kurtosis ###########################
mean_boot=np.mean(boot_stat)
confi=[np.percentile(boot_stat,conf_interval[0]*100),np.percentile(boot_stat,conf_interval[1]*100)]

print( 'RTW=',RTWs,\
      '\n mean RTW from bootstrap: ', mean_boot,\
      '\n error:',confi[0],confi[1]  )

fig=plt.figure()
fig.suptitle('Bootstraping RTW histogram \n %.1f%% confidence result:  RTW = %.3f + [%.3f,%.3f]'\
             %(confidence,RTWs,confi[0]-mean_boot,confi[1]-mean_boot))
ax=fig.add_subplot(111)
height,bin_pos=astrost.histogram(boot_stat,bins='knuth',density=True)
ax.bar(bin_pos[:-1],height,width=np.diff(bin_pos),align='edge')
ax.plot((RTWs,RTWs),(0,np.max(height)),'-',color='orange',label='Empirical RTW from %i values' %(data))   
ax.plot((mean_boot,mean_boot),(0,np.max(height)),'-r',label='Mean resampled RTW')
ax.plot((confi[0],confi[0]),(0,np.max(height)),'--g',label='error')
ax.plot((confi[1],confi[1]),(0,np.max(height)),'--g')
ax.set_xlabel('Resampled RTW')
plt.legend()


#%%
step=int(data/40)
points=np.arange(step,data+step,step)

plots=70
    
kurts=np.empty((len(points),plots), dtype='float')
ms=np.empty((len(points),plots), dtype='float')
rris=np.empty((len(points),plots), dtype='float')
kr2=np.empty((len(points),plots), dtype='float')

for j in range(plots):
    
    kurtevol=np.empty_like(points, dtype='float')
    mevol=np.zeros_like(points, dtype='float')
    rrievol=np.zeros_like(points, dtype='float')
    i=0

    for a in points:
        kurtevol[i]=stats.kurtosis(s[:a])
        datacosas=np.copy(s[:a])
        mevol[i]=metrics.RTW_max(datacosas,k)
        rrievol[i]=metrics.metric_rri(datacosas)
        kr2[i]=metrics.kr2(datacosas)

        i=i+1

    kurts[:,j]=kurtevol
    ms[:,j]=mevol
    rris[:,j]=rrievol
    
    np.random.shuffle(s)

#%%
boot_stat,boot_error = boots.bootstrap_func_len2(stats.kurtosis,s,number_of_samples,debug=True)
fig=plt.figure()
fig.suptitle('Kurtosis deviation and convergence for the Rayleigh distribution \n for %.i realizations.'\
             %(plots) )
ax=fig.add_subplot(311)
meanss=np.mean(kurts,axis=1)
for j in range(plots):
    plt.plot(points, kurts[:,j],'.',alpha=0.4)
ax.fill_between(points, meanss,meanss+np.std(kurts,axis=1),color='blue',alpha=0.7)
ax.fill_between(points, meanss,meanss-np.std(kurts,axis=1),color='blue',alpha=0.7)
ax.plot(points, kurt*np.ones_like(points),kurt*np.ones_like(points)+boot_error[conf_index[0]]*np.ones_like(points),color='green',alpha=0.9)
ax.plot(points, kurt*np.ones_like(points),kurt*np.ones_like(points)+boot_error[conf_index[1]]*np.ones_like(points),color='green',alpha=0.9)
plt.plot(points,meanss,'-',color='orange')
ax.set_ylabel('Kurtosis')
ax.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(312)
ax1.scatter(points,np.std(kurts,axis=1))
ax1.set_ylabel('$\sigma$(K)')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(313)
ax1.scatter(points,np.std(kurts,axis=1)/meanss)
ax1.set_ylabel('Relative deviation:\n $\sigma(K)/<K>$')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))
#ax1.set_ylim(top=(np.std(kurts,axis=1)[-2]/meanss[-2]))

boot_stat,boot_error = boots.bootstrap_func_len2(metrics.RTW_max,s,number_of_samples,debug=True)
fig=plt.figure()
fig.suptitle('RTW deviation and convergence for the Rayleigh distribution \n for %.i realizations.'\
             %(plots) )
ax=fig.add_subplot(311)
meanss=np.mean(ms,axis=1)
for j in range(plots):
    plt.plot(points, ms[:,j],'.',alpha=0.4)
ax.fill_between(points, meanss,meanss+np.std(ms,axis=1),color='blue',alpha=0.7)
ax.fill_between(points, meanss,meanss-np.std(ms,axis=1),color='blue',alpha=0.7)
ax.set_ylabel('RTW')
plt.plot(points,meanss,'-',color='orange')
ax.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(312)
ax1.scatter(points,np.std(ms,axis=1))
ax1.set_ylabel('$\sigma$(RTW)')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(313)
ax1.scatter(points,np.std(ms,axis=1)/meanss)
ax1.set_ylabel('Relative deviation:\n $\sigma(RTW)/<RTW>$')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))
#ax1.set_ylim(top=(np.std(kurts,axis=1)[-2]/meanss[-2]))


fig=plt.figure()
boot_stat,boot_error = boots.bootstrap_func_len2(metrics.kr2,s,number_of_samples,debug=True)
fig.suptitle('KR2 deviation and convergence for the Rayleigh distribution \n for %.i realizations.'\
             %(plots) )
ax=fig.add_subplot(311)
meanss=np.mean(kr2,axis=1)
for j in range(plots):
    plt.plot(points, kr2[:,j],'.',alpha=0.4)
ax.fill_between(points, meanss,meanss+np.std(kr2,axis=1),color='blue',alpha=0.7)
ax.fill_between(points, meanss,meanss-np.std(kr2,axis=1),color='blue',alpha=0.7)
plt.plot(points,meanss,'-',color='orange')
ax.set_ylabel('KR2')
ax.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(312)
ax1.scatter(points,np.std(kr2,axis=1))
ax1.set_ylabel('$\sigma$(Kr2)')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(313)
ax1.scatter(points,np.std(kr2,axis=1)/meanss)
ax1.set_ylabel('Relative deviation:\n $\sigma(KR2)/<KR2>$')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))



fig=plt.figure()
fig.suptitle('RRI deviation and convergence for the Rayleigh distribution \n for %.i realizations.'\
             %(plots) )
ax=fig.add_subplot(311)
boot_stat,boot_error = boots.bootstrap_func_len(metrics.metric_rri,s,number_of_samples,debug=True)
meanss=np.mean(rris,axis=1)
for j in range(plots):
    plt.plot(points, rris[:,j],'.',alpha=0.4)
ax.fill_between(points, meanss,meanss+np.std(rris,axis=1),color='blue',alpha=0.7)
ax.fill_between(points, meanss,meanss-np.std(rris,axis=1),color='blue',alpha=0.7)
plt.plot(points,meanss,'-',color='orange')
ax.set_ylabel('RRI')
ax.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(312)
ax1.scatter(points,np.std(rris,axis=1))
ax1.set_ylabel('$\sigma$(RRI)')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))
ax1=fig.add_subplot(313)
ax1.scatter(points,np.std(rris,axis=1)/meanss)
ax1.set_ylabel('Relative deviation:\n $\sigma(RRI)/<RRI>$')
ax1.set_xlabel('Number of data points')
ax1.set_xlim(right=(points[-2]))
