# -*- coding: utf-8 -*-
"""
Created on Tue Jul 10 11:18:07 2018

@author: gomel
"""


import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as stats
import threshold
import metrics

k=0.2

# =============================================================================
# rayleigh
# =============================================================================
steps=300
y=np.linspace(0.1,10,steps)
M=np.zeros(steps)
M_N=np.zeros(steps)
kur=np.zeros(steps)
stand=np.zeros(steps)
RW=np.zeros(steps)
data=150000
i=0


mu=0.
for a in y:
    s=np.random.exponential(a, data)
    M[i]=metrics.metricm(s,k)
    M_N[i]=metrics.metricm_norm(s,k)
    kur[i]=stats.kurtosis(s)+3
    stand[i]=np.std(s)
    thres=2*threshold.sig_wave_3sort(s)
    rw_count=0
    for j in s:
        if j>thres:
            rw_count=rw_count+1
#            print(rw_count)
    RW[i]=rw_count*3000/data
    i=i+1
    
#%%

fig, ax1 = plt.subplots()
fig.suptitle("Exponential M value as a funtion of $a$", fontsize=10, fontweight='bold')


color = 'tab:red'


ax1.plot(y,M,label='pareto',color='tab:orange')
ax1.plot(y,M_N,label='new',color=color)
ax1.set_xlabel('$a$')
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$M$',color=color)



ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,RW,label='RRI',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RRI$',color=color)


plt.text(-0.1,-.2, "Parameters: pareto threshold ($k$)= %.2f " % (k), fontsize=11, transform=ax2.transAxes)  
#fig2=plt.plot(np.ones(len(s)),s,'.')


#%%
fig, ax1 = plt.subplots()
fig.suptitle("exponential distribution kurtosis and RRI as a funtion of 'shape' ", fontsize=10, fontweight='bold')
#ax1 = fig.add_subplot(111)

color = 'tab:red'
ax1.plot(y,kur,label='kurtosis',color=color)
ax1.tick_params(axis='y',labelcolor=color)
ax1.set_ylabel('$K$',color=color)
ax1.set_xlabel('$a$')


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
color = 'tab:blue'
ax2.plot(y,RW,label='RRI',color=color)
ax2.tick_params(axis='y',labelcolor=color)
ax2.set_ylabel('$RRI$',color=color)

#fig.tight_layout()

#%%
fig1=plt.figure()
fig1.suptitle("Exponential kurtosis as a funtion RRI", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
plt.scatter(kur,RW,label='kurtosis')
ax2.set_xlabel('$K$')
ax2.set_ylabel('$RRI$')
plt.legend()


#%%
kr2=metrics.kr2(s)

kr3=metrics.kr3(s)
#%%
#fig1=plt.figure()
#fig1.suptitle("exponential deviation as a funtion of $shape$", fontsize=10, fontweight='bold')
#ax2 = fig1.add_subplot(111)
#plt.plot(y,stand,label='stand')
#ax2.set_xlabel('$a$')
#ax2.set_ylabel('$std$')
#plt.legend()
