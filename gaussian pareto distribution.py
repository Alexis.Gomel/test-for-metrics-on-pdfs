# -*- coding: utf-8 -*-
"""
Spyder Editor
 
RTW for lots of distributions
(old file)
This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma
import scipy.stats as stats
import threshold
import metrics 

k=0.2
#%%
# =============================================================================
# Gaussian distribution
# =============================================================================
mu, sigma = 5, 1. # mean and standard deviation
s = np.random.normal(mu, sigma, 19020)
#s[::-1].sort()
#M=metrics.metricm_norm(s,k)
#rri=metrics.metric_rri(s)

fig1=plt.figure()
fig1.suptitle("Gaussian histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
count, bins, ignored = plt.hist(s, 50)
plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi))*np.exp( - (bins - mu)**2 / (2 * sigma**2) ), linewidth=2, color='r')
plt.plot(s[np.int(k*len(s))]*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--k')
plt.plot(s[0:np.int(k*len(s))], 1/(sigma * np.sqrt(2 * np.pi))*np.exp( - (s[0:np.int(k*len(s))]- mu)**2 / (2 * sigma**2) ), linewidth=2, color='k')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
#plt.plot(np.mean(s)+2*threshold.sig_wave_m0(s)*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s)+threshold.sig_wave_m0(s)*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s)+threshold.sig_wave_3(s)*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--v', label='2*$H_{1/3}$')
absM=metrics.RTW_high_abs(s)
M=metrics.metricm(s)
maxM=metrics.RTW_max(s)

#plt.text(-0.1,-.19, "Parameters: $<x>= $ %.2f , $\sigma=$ %.5f , pareto threshold ($k$)= %.2f " % (mu, sigma, k), fontsize=11, transform=ax2.transAxes)  
#plt.text(0.7,0.9, " $M= $ %.5f " % (M), fontsize=14, transform=ax2.transAxes)  
#plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s)), fontsize=14, transform=ax2.transAxes)  

#fig2=plt.plot(np.ones(len(s)),s,'.')


# =============================================================================
# Rayleigh distribtion
# =============================================================================
#%%
mu, sigma = 0., 5.2 # mean and standard deviation
s2=np.sqrt(np.random.normal(mu, sigma, 300000)**2+np.random.normal(mu, sigma, 300000)**2)
s2[::-1].sort()

M=metrics.metricm_norm(s2,k)
rri=metrics.metric_rri(s2)


fig2=plt.figure()
fig2.suptitle("Rayleigh histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
count, bins, ignored = plt.hist(s2, 50, normed=True)
plt.plot(bins, bins*np.exp( - (bins)**2 / (2 * sigma**2) )/sigma**2, linewidth=2, color='r')
plt.plot(s2[np.int(k*len(s2))]*np.ones(20),np.linspace(0, np.mean(s2)*np.exp( - np.mean(s2)**2 / (2 * sigma**2) )/sigma**2,20),'--k')
plt.plot(s2[0:np.int(k*len(s2))],  s2[0:np.int(k*len(s2))]*np.exp( - s2[0:np.int(k*len(s2))]**2 / (2 * sigma**2) )/sigma**2, linewidth=2, color='k')
#ax2.set_xlabel('Population')
#plt.plot(2*sigma*np.ones(20),np.linspace(0, np.mean(s2)*np.exp( - np.mean(s2)**2 / (2 * sigma**2) )/sigma**2,20),'--v', label='2*$H_{m0}$')
#plt.plot(4*sigma*np.ones(20),np.linspace(0, np.mean(s2)*np.exp( - np.mean(s2)**2 / (2 * sigma**2) )/sigma**2,20),'--v', label='$H_{m0} 4 sig$')
#plt.plot(threshold.sig_wave_3(s2)*np.ones(20),np.linspace(0,  np.mean(s2)*np.exp( - np.mean(s2)**2 / (2 * sigma**2) )/sigma**2,20),'--v', label='$H_{1/3}$')
plt.plot(threshold.sig_wave_3sort(s2)*np.ones(20),np.linspace(0,  np.mean(s2)*np.exp( - np.mean(s2)**2 / (2 * sigma**2) )/sigma**2,20),'--v', label='$H_{1/3}sorted$')

plt.plot(2*threshold.sig_wave_3(s2)*np.ones(20),np.linspace(0,  np.mean(s2)*np.exp( - np.mean(s2)**2 / (2 * sigma**2) )/sigma**2,20),'--v', label='2*$H_{1/3}$')
#plt.plot(threshold.sig_wave_rms(s2)*np.ones(20),np.linspace(0,  np.mean(s2)*np.exp( - np.mean(s2)**2 / (2 * sigma**2) )/sigma**2,20),'--v', label='2*$H_{rms}$')
plt.legend()


plt.text(-0.1,-.15, "Parameters: $\sigma=$ %.2f , pareto threshold ($k$)= %.2f " % (sigma, k), fontsize=11, transform=ax2.transAxes)  
plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s2)), fontsize=14, transform=ax2.transAxes)  

#fig2=plt.plot(np.ones(len(s)),s,'.')
# =============================================================================
#   gamma distribution
# =============================================================================
#%%
ka, ti = 1.4, 1.5 #gamma's shape and scale
s3=np.random.gamma(ka, ti, 300000)
s3[::-1].sort()

M=metrics.metricm_norm(s3,k)
rri=metrics.metric_rri(s3)

fig3=plt.figure()
fig3.suptitle("Gamma distribution histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig3.add_subplot(111)
count, bins, ignored = plt.hist(s3, 80, normed=True)
plt.plot(bins, bins**(ka-1)*np.exp( - bins/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='r')
plt.plot(s3[np.int(k*len(s3))]*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--k')
plt.plot(s3[0:np.int(k*len(s3))],  s3[0:np.int(k*len(s3))]**(ka-1)*np.exp( - s3[0:np.int(k*len(s3))]/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='k')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
plt.plot(np.mean(s3)+8*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')
plt.plot(np.mean(s3)+4*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')

plt.text(-0.1,-.15, "Parameters: $shape= $ %.2f , $scale=$ %.2f , pareto threshold ($k$)= %.2f " % (ka, ti, k), fontsize=11, transform=ax2.transAxes)  
plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s3)), fontsize=14, transform=ax2.transAxes)  

# =============================================================================
# uniform
# =============================================================================
#%%
minn, maxx = 1., 2.
s4=np.random.uniform(minn, maxx, 300000)
s4[::-1].sort()
s3=np.random.uniform(minn, maxx, 300000)
rri=metrics.metric_rri(s4)


M=metrics.metricm_norm(s4,k)

fig3=plt.figure()
fig3.suptitle("Uniform distribution histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig3.add_subplot(111)
count, bins, ignored = plt.hist(s4, 80, normed=True)
plt.plot(bins, np.ones(len(bins)), linewidth=2, color='r')
plt.plot(s4[np.int(k*len(s4))]*np.ones(20),np.linspace(0, 1,20),'--k')
plt.plot(s4[0:np.int(k*len(s4))],  np.ones(len(s4[0:np.int(k*len(s4))])), linewidth=2, color='k')
plt.plot(threshold.sig_wave_3(s3)*np.ones(20),np.linspace(0, 1,20),'--r')
plt.plot(threshold.sig_wave_3sort(s3)*np.ones(20),np.linspace(0, 1,20),'--c')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
#plt.plot(np.mean(s3)+8*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s3)+4*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')

plt.text(-0.1,-.15, " $M= $ %.3f " % (M), fontsize=11, transform=ax2.transAxes)  
#plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
#plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s3)), fontsize=14, transform=ax2.transAxes)  



# =============================================================================
# weibull distribution
# =============================================================================
#%%
ka, lam = 1.01, 1.01#gamma's shape and scale
s3=lam*(-np.log(np.random.uniform(0,1, 300000)))**(1/ka)
s3[::-1].sort()

M=metrics.metricm_norm(s3,k)
rri=metrics.metric_rri(s3)

fig3=plt.figure()
fig3.suptitle("Weibull distribution histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig3.add_subplot(111)
count, bins, ignored = plt.hist(s3, 80, normed=True)
#plt.plot(bins, bins**(ka-1)*np.exp( - bins/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='r')
plt.plot(s3[np.int(k*len(s3))]*np.ones(20),np.linspace(0, (-np.log(0.5))**(1/ka),20),'--k')
#plt.plot(s3[0:np.int(k*len(s3))],  s3[0:np.int(k*len(s3))]**(ka-1)*np.exp( - s3[0:np.int(k*len(s3))]/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='k')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
#plt.plot(np.mean(s3)+8*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s3)+4*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')

#plt.text(-0.1,-.15, "Parameters: $shape= $ %.2f , $scale=$ %.2f , pareto threshold ($k$)= %.2f " % (ka, ti, k), fontsize=11, transform=ax2.transAxes)  
plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s3)), fontsize=14, transform=ax2.transAxes)  

# =============================================================================
# weibull2 distribution
# =============================================================================
#%%
ka = 50.2
s3=np.random.weibull(ka, 300000)
s3[::-1].sort()

M=metrics.metricm_norm(s3,k)
rri=metrics.metric_rri(s3)

fig3=plt.figure()
fig3.suptitle("Weibull distribution histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig3.add_subplot(111)
count, bins, ignored = plt.hist(s3, 80, normed=True)
#plt.plot(bins, bins**(ka-1)*np.exp( - bins/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='r')
#plt.plot(s3[np.int(k*len(s3))]*np.ones(20),np.linspace(0, (-np.log(0.5))**(1/ka),20),'--k')
#plt.plot(s3[0:np.int(k*len(s3))],  s3[0:np.int(k*len(s3))]**(ka-1)*np.exp( - s3[0:np.int(k*len(s3))]/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='k')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
#plt.plot(np.mean(s3)+8*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s3)+4*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')

#plt.text(-0.1,-.15, "Parameters: $shape= $ %.2f , $scale=$ %.2f , pareto threshold ($k$)= %.2f " % (ka, ti, k), fontsize=11, transform=ax2.transAxes)  
plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s3)), fontsize=14, transform=ax2.transAxes)  


#%%
# =============================================================================
# cauchy distrib
# =============================================================================
loc, scale=5.5, 1.
s3=stats.cauchy.rvs(loc, scale, size=400) 
s3[::-1].sort()

M=metrics.metricm_norm(s3,k)
rri=metrics.metric_rri(s3)

fig3=plt.figure()
fig3.suptitle("Cauchy distribution histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig3.add_subplot(111)
count, bins, ignored = plt.hist(s3, 80, normed=True)
plt.plot(bins, (1/np.pi)*(scale/((bins-loc)**2+scale**2)), linewidth=2, color='r')
#plt.plot(s3[np.int(k*len(s3))]*np.ones(20),np.linspace(0, (-np.log(0.5))**(1/ka),20),'--k')
#plt.plot(s3[0:np.int(k*len(s3))],  s3[0:np.int(k*len(s3))]**(ka-1)*np.exp( - s3[0:np.int(k*len(s3))]/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='k')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
#plt.plot(np.mean(s3)+8*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s3)+4*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')

#plt.text(-0.1,-.15, "Parameters: $shape= $ %.2f , $scale=$ %.2f , pareto threshold ($k$)= %.2f " % (ka, ti, k), fontsize=11, transform=ax2.transAxes)  
plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s3)), fontsize=14, transform=ax2.transAxes)  


#%%
###################################
#exponential
##################################


a= 1.4 #exponential shape a
s3=np.random.exponential(a, 100000)
s3[::-1].sort()

M=metrics.metricm_norm(s3,k)
rri=metrics.metric_rri(s3)

fig3=plt.figure()
fig3.suptitle("Exponential histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig3.add_subplot(111)
count, bins, ignored = plt.hist(s3, 80, normed=True)
plt.plot(bins, bins**(ka-1)*np.exp( - bins/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='r')
#plt.plot(s3[np.int(k*len(s3))]*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--k')
#plt.plot(s3[0:np.int(k*len(s3))],  s3[0:np.int(k*len(s3))]**(ka-1)*np.exp( - s3[0:np.int(k*len(s3))]/ ti )/(gamma(ka)*(ti**ka)), linewidth=2, color='k')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
#plt.plot(np.mean(s3)+8*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s3)+4*np.std(s3)*np.ones(20),np.linspace(0, np.mean(s3)**(ka-1)*np.exp( - np.mean(s3)/ ti )/(gamma(ka)*(ti**ka)),20),'--v', label='2*$H_{m0}$')

plt.text(-0.1,-.15, "Parameters: $shape= $ %.2f , $scale=$ %.2f , pareto threshold ($k$)= %.2f " % (ka, ti, k), fontsize=11, transform=ax2.transAxes)  
plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s3)), fontsize=14, transform=ax2.transAxes)  


plt.plot()


#%%

k=0.2
#%%
# =============================================================================
# Gaussian distribution
# =============================================================================
mu, sigma = 0., 0.005 # mean and standard deviation
s1 = np.random.normal(mu, sigma, 300000)
s2 =np.random.normal(mu+1., sigma, 300000)

s=list(s1)+list(s2)

s[::-1].sort()


fig1=plt.figure()
fig1.suptitle("Gaussian two peaks histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
count, bins, ignored = plt.hist(s,1000,  normed=True)
#count, bins, ignored = plt.hist(s2,  normed=True)

#plt.plot(bins, 1/(sigma * np.sqrt(2 * np.pi))*np.exp( - (bins - mu)**2 / (2 * sigma**2) ), linewidth=2, color='r')
#plt.plot(s[np.int(k*len(s))]*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--k')
#plt.plot(s[0:np.int(k*len(s))], 1/(sigma * np.sqrt(2 * np.pi))*np.exp( - (s[0:np.int(k*len(s))]- mu)**2 / (2 * sigma**2) ), linewidth=2, color='k')
#ax2.set_xlabel('Population')
#ax2.set_ylabel('|P|')
#plt.plot(np.mean(s)+2*threshold.sig_wave_m0(s)*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--v', label='2*$H_{m0}$')
#plt.plot(np.mean(s)+threshold.sig_wave_m0(s)*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--v', label='2*$H_{m0}$')
plt.plot(threshold.sig_wave_3sort(s)*np.ones(20),np.linspace(0, 1/(sigma * np.sqrt(2 * np.pi)),20),'--v', label='2*$H_{1/3}$')


M=metrics.metricm_norm(s,k)
rri=metrics.metric_rri(s)
