

# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 15:08:13 2020

@author: gomel
"""




import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
import matplotlib.ticker as ticker




gaus_data=np.random.normal(-1,2,10000)
I2=gaus_data-np.min(gaus_data)


x=I2
u5=np.percentile(x,50)
U5=np.mean(x[x>=u5])
# i=len(U5)
# U5=U5/i
L5=np.mean(x[x<=u5])
# k=len(L5)
# L5=L5/k

minx=min(x)
maxx=max(x)

KR_num=20 #lkr and ukr shuold not be the max or minimum.. for large tails and low datasets this can happen.
ukr=np.percentile(x,100-KR_num)
lkr=np.percentile(x,KR_num)
if minx==lkr or ukr==maxx: print('Minimum or maximum equal to percentile error. ')

#this version uses <= and >= to have it well defined always. 
UKR=np.mean(x[x>=ukr])
# i=len(U5)
# U5=U5/i
LKR=np.mean(x[x<=lkr])
# k=len(L5)
# LKR=LKR/k
# UKR=UKR/i

'''set up pcolor figures with percentiles'''
fig2=plt.figure(figsize=(15,5))
ax2=fig2.add_subplot(1,1,1)
height,bin_pos=astrost.histogram(gaus_data,bins='scott')
ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='grey',align='edge',alpha=0.35)
# ax2.plot([0,0],[0,max(height)],'k')
ax2.set_xlim([-8,15])

# ax2=fig2.add_subplot(2,1,2)
ax2.bar(bin_pos[:-1]-np.min(gaus_data),height,width=np.diff(bin_pos),color='grey',align='edge',alpha=0.8)
plt.annotate('',
             [np.mean(gaus_data),np.max(height)],fontsize='x-large',
             xytext=(np.mean(I2), np.max(height)) ,
             arrowprops=dict(arrowstyle="<-"),va="center")
ax2.set_xlim([-8,15])
ax2.plot([0,0],[0,0.9*max(height)],'--k',alpha=0.7)
# ax2.plot([LKR,LKR],[0,max(height)],'--m')
# ax2.plot([U5,U5],[0,max(height)],'--b')
# ax2.plot([L5,L5],[0,max(height)],'--b')
bin_pos=bin_pos-np.min(gaus_data)
bin_bool=bin_pos>ukr
ax2.bar(bin_pos[bin_bool][:-1],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool]),color='magenta',align='edge',alpha=0.4)
# bin_bool=bin_pos<lkr
# ax2.bar(bin_pos[bin_bool],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool])[0],color='magenta',align='edge',alpha=0.3)
ax2.xaxis.set_major_locator(ticker.FixedLocator([np.mean(gaus_data),0,np.mean(I2),ukr]))
ax2.xaxis.set_major_formatter(ticker.FixedFormatter(['$<\!I\!>$','$0$','$<\!(I-I_N)\!>$','$p(k=0.2)$']))
plt.xticks(fontsize=11)

ax2.set_yticks([])
#%%

# r_std=0.03
# j=0
# num_points=1000
# fig2.suptitle('Statistics pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the %.1f and %.1f percentiles ' %(r, r_std,num_points,std,perc,100-perc), fontsize=11, fontweight='bold')

# t_fin=40
# for n,mean in enumerate(test_points):
#     j=j+1 
#     print(mean)
#     initials=[]
#     rs=[]
#     results=[]
#     for i in range(0,num_points):
#         xs = np.linspace(0,t_fin,2)
#         y0 = np.random.normal(mean,std,2)  # the initial condition
#         param=(np.random.normal(r,r_std,1),)
#         ys = odeint(f_p, y0[-1],xs,param)
#         ys = np.array(ys).flatten()
#         results.append(ys[-1])

#     num,bin_pos=astrost.histogram(results,bins='scott')

#     pcolor_matrix[n,:]=num/np.max(num)
#     pcolor_logmatrix[n,:]=np.log10(num/np.max(num))
#     avg_stat_bin[n]=np.median(results)
#     perc_stat_bin_below[n]=np.percentile(results,100-perc)
#     perc_stat_bin_above[n]=np.percentile(results,perc)
    

# ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
# ax2.set_ylabel('$<y_0>$')