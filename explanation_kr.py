# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 15:08:13 2020

@author: gomel
"""




import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
import matplotlib.ticker as ticker




gaus_data=np.random.normal(0,2,10000)


x=gaus_data
u5=np.percentile(x,50)
U5=np.mean(x[x>=u5])
# i=len(U5)
# U5=U5/i
L5=np.mean(x[x<=u5])
# k=len(L5)
# L5=L5/k

minx=min(x)
maxx=max(x)

KR_num=5 #lkr and ukr shuold not be the max or minimum.. for large tails and low datasets this can happen.
ukr=np.percentile(x,100-KR_num)
lkr=np.percentile(x,KR_num)
if minx==lkr or ukr==maxx: print('Minimum or maximum equal to percentile error. ')

#this version uses <= and >= to have it well defined always. 
UKR=np.mean(x[x>=ukr])
# i=len(U5)
# U5=U5/i
LKR=np.mean(x[x<=lkr])
# k=len(L5)
# LKR=LKR/k
# UKR=UKR/i


'''set up pcolor figures with percentiles'''
fig2=plt.figure(figsize=(10,10))
ax2=fig2.add_subplot(1,1,1)
height,bin_pos=astrost.histogram(gaus_data,bins='scott')
ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='grey',align='edge',alpha=0.8)
ax2.plot([UKR,UKR],[0,max(height)],'--m')
ax2.plot([LKR,LKR],[0,max(height)],'--m')
ax2.plot([U5,U5],[0,max(height)],'--b')
ax2.plot([L5,L5],[0,max(height)],'--b')
bin_bool=bin_pos>ukr
ax2.bar(bin_pos[bin_bool][:-1],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool]),color='magenta',align='edge',alpha=0.3)
bin_bool=bin_pos<lkr
ax2.bar(bin_pos[bin_bool],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool])[0],color='magenta',align='edge',alpha=0.3)


ax2.xaxis.set_major_locator(ticker.FixedLocator([lkr,u5,ukr]))
ax2.xaxis.set_major_formatter(ticker.FixedFormatter(['$0.2$','$0.5$','$0.8$']))

# ax2.set_xticks([lkr,u5,ukr],['1','3','4'])

#%%

'''set up pcolor figures with percentiles'''
fig2=plt.figure(figsize=(10,4))
ax2=fig2.add_subplot(1,1,1)
height,bin_pos=astrost.histogram(gaus_data,bins='scott')
ax2.bar(bin_pos[:-1],height,width=np.diff(bin_pos),color='grey',align='edge',alpha=0.8)
ax2.plot([UKR,UKR],[0,max(height)],'--m')
ax2.plot([LKR,LKR],[0,max(height)],'--m')
ax2.plot([U5,U5],[0,max(height)],'--b')
ax2.plot([L5,L5],[0,max(height)],'--b')
# bin_bool=bin_pos>ukr
# ax2.bar(bin_pos[bin_bool][:-1],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool]),color='magenta',align='edge',alpha=0.3)
# bin_bool=bin_pos<lkr
# ax2.bar(bin_pos[bin_bool],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool])[0],color='magenta',align='edge',alpha=0.3)


bin_bool=np.logical_and(bin_pos>L5,bin_pos<U5)
ax2.bar(bin_pos[bin_bool],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool])[0],color='blue',align='edge',alpha=0.6)

bin_bool=np.logical_and(bin_pos>LKR,bin_pos<UKR)
ax2.bar(bin_pos[bin_bool],height[bin_bool[:-1]],width=np.diff(bin_pos[bin_bool])[0],color='magenta',align='edge',alpha=0.3)
ax2.xaxis.set_major_locator(ticker.FixedLocator([LKR,lkr,L5,u5,U5,ukr,UKR]))
ax2.xaxis.set_major_formatter(ticker.FixedFormatter(['$L_{0.2}$','$0.2$','$L_{0.5}$','$0.5$','$U_{0.5}$','$0.8$','$U_{0.2}$']))
plt.xticks(fontsize=16)
# ax2.set_xticks([lkr,u5,ukr],['1','3','4'])



# std=0.1
# lims=0.4
# amount_of_stats=15
# binss=np.linspace(-0.6,0.6,70)

# pcolor_matrix=np.zeros((amount_of_stats,len(binss)-1))
# pcolor_logmatrix=np.zeros((amount_of_stats,len(binss)-1))
# avg_stat_bin=np.zeros((amount_of_stats))
# perc_stat_bin_below=np.zeros((amount_of_stats))
# perc_stat_bin_above=np.zeros((amount_of_stats))
# perc=90#has to be grater than 50

# testlim=.35
# test_points=np.linspace(-testlim,testlim,amount_of_stats)
# r=0.1
# r_std=0.03
# j=0
# num_points=1000
# fig2.suptitle('Statistics pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the %.1f and %.1f percentiles ' %(r, r_std,num_points,std,perc,100-perc), fontsize=11, fontweight='bold')

# t_fin=40
# for n,mean in enumerate(test_points):
#     j=j+1 
#     print(mean)
#     initials=[]
#     rs=[]
#     results=[]
#     for i in range(0,num_points):
#         xs = np.linspace(0,t_fin,2)
#         y0 = np.random.normal(mean,std,2)  # the initial condition
#         param=(np.random.normal(r,r_std,1),)
#         ys = odeint(f_p, y0[-1],xs,param)
#         ys = np.array(ys).flatten()
#         results.append(ys[-1])

#     num,bin_pos=astrost.histogram(results,bins='scott')

#     pcolor_matrix[n,:]=num/np.max(num)
#     pcolor_logmatrix[n,:]=np.log10(num/np.max(num))
#     avg_stat_bin[n]=np.median(results)
#     perc_stat_bin_below[n]=np.percentile(results,100-perc)
#     perc_stat_bin_above[n]=np.percentile(results,perc)
    

# ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
# ax2.set_ylabel('$<y_0>$')