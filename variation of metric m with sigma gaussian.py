# -*- coding: utf-8 -*-
"""
Created on Fri May 18 17:46:57 2018

@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
import metrics

k=0.2

# =============================================================================
# Gaussian distribution
# =============================================================================
steps=900
mu = 10. #standard deviation
y=np.linspace(0.0,200.0,steps)
M=np.zeros(steps)
M_N=np.zeros(steps)
i=0
for sigma in y:
    s = np.random.normal(mu, sigma, 50000)
    M[i]=metrics.metricm(s,k)
    M_N[i]=metrics.metricm_norm(s,k)
    i=i+1


fig1=plt.figure()
fig1.suptitle("Gaussian M value as a funtion of $\sigma$ with mean= %.2f" %(mu), fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
#plt.plot(y,M,label='pareto' )
plt.plot(y,M_N,label='normalized')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('M')
plt.legend()
plt.text(-0.1,-.2, "Parameters: $\mu=$ %.2f , pareto threshold ($k$)= %.2f " % (mu, k), fontsize=11, transform=ax2.transAxes)  
#fig2=plt.plot(np.ones(len(s)),s,'.')
