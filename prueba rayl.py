# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 13:37:46 2018

@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.special import gamma
import scipy.stats as stats
import threshold
from metrics import metricm as metricm

k=0.2

"""rayleigh"""
mu, sigma = 0., 4.5 # mean and standard deviation
s2=np.sqrt(np.random.normal(mu, sigma, 300000)**2+np.random.normal(mu, sigma, 300000)**2)
s2[::-1].sort()

M=metricm(s2,k)







"""weibull"""

ka, lam = 0.51, 0.61#gamma's shape and scale
s3=lam*(-np.log(np.random.uniform(0,1, 300000)))**(1/ka)
s3[::-1].sort()

"""gauss"""
mu, sigma = 0., 4.5 # mean and standard deviation
s = np.random.normal(mu, sigma, 300000)
s[::-1].sort()
M=metricm(s,k)


"""plot"""
fig2=plt.figure()
fig2.suptitle("Rayleigh histogram and threshold for the pareto definition ", fontsize=10, fontweight='bold')
ax2 = fig2.add_subplot(111)
#plt.plot(s/np.mean(s),np.arange(len(s)),'*k')
plt.plot(s2/np.mean(s2),np.arange(len(s2)),'*r')
plt.plot(s3/np.mean(s3),np.arange(len(s3)),'*b')
plt.yscale('log')
plt.text(-0.1,-.15, "Parameters: $\sigma=$ %.2f , pareto threshold ($k$)= %.2f " % (sigma, k), fontsize=11, transform=ax2.transAxes)  
plt.text(0.7,0.9, " $M= $ %.3f " % (M), fontsize=14, transform=ax2.transAxes)  
plt.text(0.7,0.7, " $Kurt= $ %.3f " % (stats.kurtosis(s2)), fontsize=14, transform=ax2.transAxes)  
