# -*- coding: utf-8 -*-
"""
Created on Wed May 16 18:06:47 2018

estudio de cambio de rayleigh cuando muevo .
@author: gomel
"""


import numpy as np
import matplotlib.pyplot as plt

import metrics

k=0.2

# =============================================================================
# Rayleigh distribution
# =============================================================================
steps=500
y=np.linspace(0.1,15000.0,steps)
M=np.zeros(steps)
kr2=np.zeros(steps)
kr3=np.zeros(steps)
Rrms=np.zeros(steps)
Rr=np.zeros(steps)
i=0
mu=0.
for sigma in y:
    s=np.sqrt(np.random.normal(mu, sigma, 40000)**2+np.random.normal(mu, sigma, 40000)**2)
    M[i]=metrics.RTW_max(s,k)
    kr2[i]=metrics.kr2(s)
    kr3[i]=metrics.kr3(s)
    Rrms[i]=metrics.metric_rms_tld(s)
    Rr[i]=metrics.metric_rc_tld(s)
    i=i+1

fig1=plt.figure()
fig1.suptitle("Rayleigh M value as a funtion of $\sigma$", fontsize=10, fontweight='bold')
ax2 = fig1.add_subplot(111)
plt.plot(y,M)
plt.plot(y,kr2,label='normalized')
ax2.set_xlabel('$\sigma$')
ax2.set_ylabel('M')

plt.text(-0.1,-.2, "Parameters: pareto threshold ($k$)= %.2f " % (k), fontsize=11, transform=ax2.transAxes)  
#fig2=plt.plot(np.ones(len(s)),s,'.')
